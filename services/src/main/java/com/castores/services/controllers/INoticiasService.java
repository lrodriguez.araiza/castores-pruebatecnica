/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castores.services.controllers;

import com.castores.services.models.Usuario;
import com.castores.services.models.Noticia;
import com.castores.services.models.Comentario;
import java.util.List;

/**
 *
 * @author Equipo3
 */
public interface INoticiasService {
    List<Comentario> obtenerComentariosPorNoticia(int id_noticia);
    void guardarComentario(Comentario comentario);
    List<Noticia> obtenerNoticias();
    Noticia guardarNoticia(Noticia noticia);
    Usuario guardarUsuario(Usuario usuario);
    Usuario login(String nickname, String password);
    void asignarUsuario(Usuario usuario);
    Usuario obtenerUsuario(int id);

}

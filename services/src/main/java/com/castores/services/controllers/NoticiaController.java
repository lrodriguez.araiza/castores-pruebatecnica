/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castores.services.controllers;

import com.castores.services.models.Comentario;
import com.castores.services.models.Noticia;
import com.castores.services.models.Usuario;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Equipo3
 */
@CrossOrigin(origins = "*",maxAge=3600)
@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class NoticiaController {
    
    @Autowired
    NoticiaService service;
    
    /*@PostMapping
    public void Registrar(Usuario usuario){
        service.guardarUsuario(usuario);
    }*/
    
    @GetMapping("/noticias")
    public List<Noticia> Noticias(){
        return service.obtenerNoticias();
    }
    
    @PostMapping("/login")
    public Usuario Login(String username, String password){
        try{
            return service.login(username, password);
        }
        catch(Exception ex){
           System.out.println("Un error ha ocurrido: "+ ex.getMessage());
           return null;
        }
       
    }
    
    @PostMapping("/registrar")
    public Usuario Registrar(@RequestBody Usuario usuario){
        try{
            usuario.setCreacion(LocalDateTime.now());
            return service.guardarUsuario(usuario);
        }
        catch(Exception ex){
           System.out.println("Un error ha ocurrido: "+ ex.getMessage());
            return null;
        }
    }
    
    @PostMapping("/crearNoticia")
    public Noticia GuardarNoticia(@RequestBody Noticia noticia){
        try{
            if (service.obtenerUsuario(noticia.getFk_usuario()).getFk_personal() != null){
                return service.guardarNoticia(noticia);
            }
            else{
                return null;
            }
        }
        catch(Exception ex){
            System.out.println("Un error ha ocurrido: "+ ex.getMessage());
            return null;        }
    }
    
    @PostMapping("/comentarios/agregar")
    public Map<String, Boolean> GuardarComentario(@RequestBody Comentario comentario){
        try{
            service.guardarComentario(comentario);
            return Collections.singletonMap("created", true );
        }
        catch(Exception ex){
           return Collections.singletonMap("Un error ha ocurrido: "+ ex.getMessage() ,false);
        }
    }
    
        
    @GetMapping("/comentarios")
    public List<Comentario> ComentariosByNoticia(@RequestParam Integer id){
        return service.obtenerComentariosPorNoticia(id);
    }
}

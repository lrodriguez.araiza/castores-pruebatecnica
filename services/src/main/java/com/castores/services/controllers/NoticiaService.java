/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castores.services.controllers;

import com.castores.services.models.Usuario;
import com.castores.services.models.Noticia;
import com.castores.services.models.Comentario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.castores.services.repos.UsuarioRepository;
import com.castores.services.repos.NoticiaRepository;
import com.castores.services.repos.ComentarioRepository;

/**
 *
 * @author Equipo3
 */
@Service
public class NoticiaService implements INoticiasService {

    @Autowired
    private ComentarioRepository comentarios;
    @Autowired
    private NoticiaRepository noticias;
    @Autowired
    private UsuarioRepository usuarios;

    @Override
    public List<Comentario> obtenerComentariosPorNoticia(int id_noticia) {
        return null;// comentarios.findByForeign(id_noticia);
    }

    @Override
    public void guardarComentario(Comentario comentario) {
        comentarios.save(comentario);
    }

    @Override
    public List<Noticia> obtenerNoticias() {
        return noticias.findAll();
    }

    @Override
    public Noticia guardarNoticia(Noticia noticia) {
        return noticias.save(noticia);
    }

    @Override
    public Usuario guardarUsuario(Usuario usuario) {
        return usuarios.save(usuario);
    }

    @Override
    public Usuario login(String username, String password) {
        return usuarios.login(username, password);
    }

    @Override
    public void asignarUsuario(Usuario usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario obtenerUsuario(int id) {
        return usuarios.findById(id).get();
    }
    
}

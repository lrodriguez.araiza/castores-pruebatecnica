/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castores.services.models;

import java.sql.Time;
import javax.persistence.*;

/**
 *
 * @author Equipo3
 */
@Entity
@Table(name = "comentario")
public class Comentario {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
        
    @Column
    private String contenido;
    
    @Column
    private Integer comentario_superior;
    
    @Column
    private Time creacion;    
    
    @Column
    private int fk_noticia;
   
    @Column
    private Integer fk_usuario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Integer getComentario_superior() {
        return comentario_superior;
    }

    public void setComentario_superior(Integer comentario_superior) {
        this.comentario_superior = comentario_superior;
    }

    public Time getCreacion() {
        return creacion;
    }

    public void setCreacion(Time creacion) {
        this.creacion = creacion;
    }

    public Integer getFk_noticia() {
        return fk_noticia;
    }

    public void setFk_noticia(Integer fk_noticia) {
        this.fk_noticia = fk_noticia;
    }

    public Integer getFk_usuario() {
        return fk_usuario;
    }

    public void setFk_usuario(Integer fk_usuario) {
        this.fk_usuario = fk_usuario;
    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castores.services.models;

import java.sql.Time;
import java.time.LocalDateTime;
import javax.persistence.*;

/**
 *
 * @author Equipo3
 */
@Entity
@Table(name = "usuario")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
        
    @Column
    private String nickname;

    @Column
    private String password;
        
    @Column
    private String email;
            
    @Column
    private String nombre;
                
    @Column
    private LocalDateTime creacion;
    
    @Column
    private Integer fk_personal = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDateTime getCreacion() {
        return creacion;
    }

    public void setCreacion(LocalDateTime creacion) {
        this.creacion = creacion;
    }

    public Integer getFk_personal() {
        return fk_personal;
    }

    public void setFk_personal(Integer fk_personal) {
        this.fk_personal = fk_personal;
    }
    
    
}

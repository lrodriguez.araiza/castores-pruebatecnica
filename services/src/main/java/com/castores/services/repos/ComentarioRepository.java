/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castores.services.repos;

import com.castores.services.models.Comentario;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Equipo3
 */ 
@Repository
public interface ComentarioRepository extends JpaRepository<Comentario, Integer>{
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castores.services.repos;

import com.castores.services.models.Usuario;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Equipo3
 */
@Transactional(readOnly = true)
public class UsuarioRepositoryImpl  {

    @PersistenceContext
    EntityManager entityManager;

    public Usuario login(String username, String password) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Usuario> criteria = criteriaBuilder.createQuery(Usuario.class);
        Root<Usuario> root = criteria.from(Usuario.class);
        
        Predicate predicateFoNickname = criteriaBuilder.equal(root.get("nickname"), username);
        Predicate predicateForEmail = criteriaBuilder.equal(root.get("email"), username);
        Predicate predicateForUsername = criteriaBuilder.or(predicateFoNickname, predicateForEmail);
        Predicate predicateForPassword = criteriaBuilder.equal(root.get("password"), password);
        Predicate predicateForLogin = criteriaBuilder.or(predicateForUsername, predicateForPassword);


        criteria.where(
            predicateForLogin
        );
       

        return entityManager
        .createQuery(criteria)
        .getResultList().stream().findFirst().orElse(null);
        
    }
    
}

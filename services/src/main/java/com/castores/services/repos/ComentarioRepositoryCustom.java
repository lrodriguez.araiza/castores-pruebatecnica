/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.castores.services.repos;

import com.castores.services.models.Comentario;
import java.util.List;

/**
 *
 * @author Equipo3
 */
public interface ComentarioRepositoryCustom {
    List<Comentario> findByIdNoticia(int idNoticia); 
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NoticiasService } from 'src/app/Servicio/noticias.service';
import { Noticia } from 'src/app/Modelo/Noticia'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  noticias: Noticia[]

  constructor(private router:Router, private service:NoticiasService) { }

  ngOnInit(): void {
    this.Noticias()
  }

  Noticias(){
    this.service.obtenerNoticias()
    .subscribe(data=>{
      this.noticias = data
    })
  }

  CrearNoticia(){
    this.router.navigate(['nueva_noticia']);
  }

}

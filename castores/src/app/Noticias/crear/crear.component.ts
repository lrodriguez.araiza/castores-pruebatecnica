import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Noticia } from 'src/app/Modelo/Noticia';
import { NoticiasService } from 'src/app/Servicio/noticias.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.css']
})
export class CrearComponent implements OnInit {


  noticia: Noticia = new Noticia()

  constructor(private router:Router, private service:NoticiasService) { }

  ngOnInit(): void {
    this.resetForm()
  }

  resetForm(form? : NgForm){
    if(form != null){
      form.resetForm();
      this.noticia = {
        id : 0,
        imagen : "",
        titulo : "",
        contenido : "",
        fk_usuario : 0
      }
    }
  }

  Registrar(form:NgForm){
    this.service.crearNoticia(form.value)
    .subscribe(data=>{
      this.router.navigate(['noticias'])
    })
  }

}

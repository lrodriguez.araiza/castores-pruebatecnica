import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NoticiasService } from 'src/app/Servicio/noticias.service';
import { Login } from 'src/app/Modelo/Login'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login: Login = new Login()

  constructor(private router:Router, private service:NoticiasService) { }

  ngOnInit(): void {
    this.resetForm()
  }

  resetForm(form? : NgForm){
    if(form != null){
      form.resetForm();
      this.login = {
        nickname : "",
        password : ""
      }
    }
  }

  Login(form:NgForm){
    this.service.login(form.value)
    .subscribe(data=>{
      this.router.navigate(['noticias'])
    })
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NoticiasService } from 'src/app/Servicio/noticias.service';
import { Usuario } from 'src/app/Modelo/Usuario'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  usuario: Usuario = new Usuario()

  constructor(private router:Router, private service:NoticiasService) { }

  ngOnInit(): void {
    this.resetForm()
  }

  resetForm(form? : NgForm){
    if(form != null){
      form.resetForm();
      this.usuario = {
        email : "",
        id : 0,
        nickname : "",
        nombre : "",
        password : "",
        fk_personal : 0
      }
    }
  }

  Registrar(form:NgForm){
    this.service.registrar(form.value)
    .subscribe(data=>{
      this.router.navigate(['login'])
    })
  }

}

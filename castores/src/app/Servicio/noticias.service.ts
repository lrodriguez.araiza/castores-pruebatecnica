import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Usuario } from '../Modelo/Usuario'
import { Login } from '../Modelo/Login'
import { Noticia } from '../Modelo/Noticia';

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {

  constructor(private http:HttpClient) { }

  readonly Url='http://localhost:8080/api/';

  login(login:Login){
    return this.http.post<Login>(this.Url + "login",login);
  }

  registrar(usuario:Usuario){
    const body: Usuario = {
      email : usuario.email,
      id : usuario.id,
      nickname : usuario.nickname,
      nombre : usuario.nombre,
      password : usuario.password,
      fk_personal : usuario.fk_personal
    }
    return this.http.post<Usuario>(this.Url + "registrar",body);
  }

  obtenerNoticias(){
    return this.http.get<Noticia[]>(this.Url + "noticias")
  }

  crearNoticia(noticia:Noticia){
    const body: Noticia = {
      id : noticia.id,
      imagen : noticia.imagen,
      titulo : noticia.titulo,
      contenido : noticia.contenido,
      fk_usuario : 26//noticia.fk_usuario
    }
    return this.http.post<Noticia>(this.Url + "crearNoticia",body);
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaComponent } from './Noticias/lista/lista.component';
import { CrearComponent } from './Noticias/crear/crear.component';
import { RegistrarComponent } from './Usuario/registrar/registrar.component';
import { LoginComponent } from './Usuario/login/login.component';
import { RegistroPersonalComponent } from './Usuario/registro-personal/registro-personal.component';
import { NoticiasService } from './Servicio/noticias.service';

@NgModule({
  declarations: [
    AppComponent,
    ListaComponent,
    CrearComponent,
    RegistrarComponent,
    LoginComponent,
    RegistroPersonalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [NoticiasService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaComponent } from './Noticias/lista/lista.component';
import { RegistrarComponent } from './Usuario/registrar/registrar.component';
import { LoginComponent } from './Usuario/login/login.component';
import { CrearComponent } from './Noticias/crear/crear.component';


const routes: Routes = [
  {path:'noticias',component:ListaComponent},
  {path:'registrar',component:RegistrarComponent},
  {path:'login',component:LoginComponent},
  {path:'nueva_noticia',component:CrearComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
